import { Alert, StyleSheet, Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { LongPressGestureHandler, State, TapGestureHandler } from 'react-native-gesture-handler';

const Home = () => {
  const navigation = useNavigation();
  const onLongPress = (e) => {
    if (e.nativeEvent.state === State.ACTIVE) {
      navigation.navigate('Game');
    }
  };
  const onTap = (e) => {
    if (e.nativeEvent.state === State.ACTIVE) {
      Alert.alert('Long press to start the game');
    }
  };

  return (
    <View style={styles.container}>
      <TapGestureHandler onHandlerStateChange={onTap}>
        <LongPressGestureHandler onHandlerStateChange={onLongPress} minDurationMs={600}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Start game!</Text>
          </View>
        </LongPressGestureHandler>
      </TapGestureHandler>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: 300,
    height: 300,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: 150,
    backgroundColor: 'purple',
  },
  buttonText: {
    color: 'white',
    fontSize: 48,
  },
});

export default Home;
