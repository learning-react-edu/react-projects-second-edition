import { StyleSheet, Text, View } from 'react-native';
import { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import AnimatedButton from '../components/AnimatedButton';

export const GameChoices = Object.freeze({
  HIGHER: 'higher',
  LOWER: 'lower',
});

const Game = () => {
  const baseNumber = Math.floor(Math.random() * 100);
  const score = Math.floor(Math.random() * 100);
  const [ choice, setChoice ] = useState('');
  const navigation = useNavigation();

  useEffect(() => {
    if (choice) {
      const winner =
        (choice === GameChoices.HIGHER && score > baseNumber) ||
        (choice === GameChoices.LOWER && score < baseNumber);
      navigation.navigate('Result', { winner });
    }
  }, [baseNumber, score, choice]);

  return (
    <View style={styles.container}>
      <Text style={styles.baseNumber}>Starting: {baseNumber}</Text>
      <AnimatedButton action={GameChoices.HIGHER} onPress={() => setChoice(GameChoices.HIGHER)} />
      <AnimatedButton action={GameChoices.LOWER} onPress={() => setChoice(GameChoices.LOWER)} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  baseNumber: {
    fontSize: 48,
    marginBottom: 30,
  },
  button: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: 15,
    padding: 30,
    marginVertical: 15,
  },
  buttonRed: {
    backgroundColor: 'red',
  },
  buttonGreen: {
    backgroundColor: 'green',
  },
  buttonText: {
    color: 'white',
    fontSize: 24,
  },
});

export default Game;
