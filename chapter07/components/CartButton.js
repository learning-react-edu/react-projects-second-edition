import { gql, useQuery } from '@apollo/client';
import { Button } from './Button';

const GET_CART_TOTAL = gql`
  query getCart {
    cart {
      count
    }
  }
`;

const CartButton = ({ ...props }) => {
  const { loading, data } = useQuery(GET_CART_TOTAL);

  return (
    <Button {...props}>
      {loading ? 'Cart' : `Cart (${data.cart.count})`}
    </Button>
  );
};

export {
  CartButton,
  GET_CART_TOTAL,
};
