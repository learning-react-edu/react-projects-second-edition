import { gql, useQuery } from '@apollo/client';
import styled from 'styled-components';
import { ProductItem, SubHeader } from '../../components';

const ProductItemsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  flex-direction: column;
  margin: 2% 5%;
`;

const GET_PRODUCTS = gql`
  query getProducts {
    products {
      id
      title
      price
      thumbnail
    }
  }
`;

const Products = () => {
  const { loading, data } = useQuery(GET_PRODUCTS);

  return (
    <>
      <SubHeader title='Available products' goToCart/>
      { loading ? (
        <span>Loading...</span>
      ) : (
        <ProductItemsWrapper>
          { data && data.products && data.products.map(product => (
            <ProductItem
              key={product.id}
              data={product}
              addToCart
            />
          ))}
        </ProductItemsWrapper>
      )}
    </>
  );
};

export default Products;
