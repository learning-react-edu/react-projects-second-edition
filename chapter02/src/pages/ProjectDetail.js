import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

const ProjectDetail = ({ userName }) => {
  const [ loading, setLoading ] = useState(false);
  const [ project, setProject ] = useState([]);
  const { name } = useParams();

  useEffect(() => {
    const fetchData = async () => {
      const data = await fetch(`https://api.github.com/repos/${userName}/${name}`);
      const project = await data.json();
      if (project) {
        setProject(project);
        setLoading(false);
      }
    };
    if (userName && name) {
      fetchData();
    }
  }, [userName, name]);

  return (
    <div className='Project-container'>
      <h2>Project: {project.name}</h2>
      { loading ? (
        <span>Loading...</span>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export {
  ProjectDetail,
};
