import { useEffect, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { List } from '../components/List';

const Projects = ({ userName }) => {
  const [ loading, setLoading ] = useState(true);
  const [ projects, setProjects ] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      const data = await fetch(`https://api.github.com/users/${userName}/repos`);
      const projects = await data.json();
      if (projects) {
        setProjects(projects);
        setLoading(false);
      }
    };
    fetchData();
  }, [userName]);

  return (
    <div className='Projects-container'>
      <h2>Projects</h2>
      { loading ? (
        <span>Loading...</span>
      ) : (
        <>
          <List items={projects.map(project => ({
            field: project.name,
            value: <RouterLink to={`/projects/${project.name}`}>
              Open project
            </RouterLink>,
          }))} />
        </>
        )}
    </div>
  );
};

export {
  Projects,
};
