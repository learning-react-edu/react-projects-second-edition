import { ListsContextProvider } from './ListsContext';
import { ItemsContextProvider } from './ItemsContext';

const AppContext = ({ children }) => (
  <ListsContextProvider>
    <ItemsContextProvider>
      {children}
    </ItemsContextProvider>
  </ListsContextProvider>
);

export {
  AppContext,
};
