import { useEffect, useState } from 'react';

const useDataFetching = (dataSource) => {
  const [ loading, setLoading ] = useState(false);
  const [ data, setData ] = useState([]);
  const [ error, setError ] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        const tasks = await fetch(dataSource);
        const result = await tasks.json();
        if (result) {
          setData(result);
          setLoading(false);
        }
      } catch (e) {
        setLoading(false);
        setError(e.message);
      }
    };
    fetchData();
  }, [ dataSource ]);
  return [ loading, error, data ];
};

export {
  useDataFetching,
};
