import './Header.css';

const Header = () => (
  <div className='Header-wrapper'>
    <h1>Project Management Board</h1>
  </div>
);

export {
  Header,
};
