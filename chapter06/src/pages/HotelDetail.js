import { useContext, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import styled from 'styled-components';
import { HotelItem, NavBar, ReviewItem } from '../components';
import { HotelsContext, ReviewsContext } from '../context';

const ReviewsItemWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  margin: 2% 5%;
`;

const HotelDetail = () => {
  let navigate = useNavigate();
  const { hotelId } = useParams();

  const { loading, error, hotel, fetchHotel } = useContext(HotelsContext);
  const { reviews, fetchReviews } = useContext(ReviewsContext);

  useEffect(() => {
    hotelId && !hotel.id && fetchHotel(hotelId);
  }, [ fetchHotel, hotel.id, hotelId ]);

  useEffect(() => {
    hotelId && !reviews.length && fetchReviews(hotelId);
  }, [ fetchReviews, hotelId, reviews.length ]);

  return (
    <>
      {navigate && (
        <NavBar
          goBack={() => navigate(-1)}
          openForm={() => navigate(`/hotel/${hotelId}/new`)}
          title={hotel && hotel.title}
        />
      )}
      {hotel && <HotelItem data={hotel}/>}
      <ReviewsItemWrapper>
        {loading || error ? (
          <span>{error || 'Loading...'}</span>
        ) : (
          reviews.map((review) => <ReviewItem key={review.id} data={review}/>)
        )}
      </ReviewsItemWrapper>
    </>
  );
};

export {
  HotelDetail,
};
