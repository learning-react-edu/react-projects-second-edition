import { render, screen, waitFor } from '@testing-library/react';
import { HotelsContext } from '../context';
import { Hotels } from './Hotels';
import { BrowserRouter } from 'react-router-dom';

test('The Hotels component should render', async () => {
  const wrapper = ({ children }) => (
    <HotelsContext.Provider
      value={{
        loading: true,
        error: '',
        hotels: [],
        fetchHotels: jest.fn(),
      }}
    >
      {children}
    </HotelsContext.Provider>
  );
  render(<Hotels />, { wrapper });
  expect(await screen.findByText('Loading...')).toBeVisible();
});

test('The Hotels component should render a list of hotels', async () => {
  const wrapper = ({ children }) => (
    <BrowserRouter>
      <HotelsContext.Provider
        value={{
          loading: false,
          error: '',
          hotels: [
            { id: 1, title: 'Test hotel 1', thumbnail: ''},
            { id: 2, title: 'Test hotel 2', thumbnail: ''},
          ],
          fetchHotels: jest.fn(),
        }}
      >
        {children}
      </HotelsContext.Provider>
    </BrowserRouter>
  );
  render(<Hotels />, { wrapper });
  expect(screen.queryByText('Loading...')).toBeNull();
  expect(screen.getAllByRole('link')).toHaveLength(2);
});

test('The Hotels component should fetch hotels when no hotels in the context', async () => {
  const mockFunction = jest.fn();
  const wrapper = ({ children }) => (
    <HotelsContext.Provider
      value={{
        loading: true,
        error: '',
        hotels: [],
        fetchHotels: mockFunction,
      }}
    >
      {children}
    </HotelsContext.Provider>
  );
  render(<Hotels />, { wrapper });
  await waitFor(() => expect(mockFunction).toHaveBeenCalledTimes(1));
});

test('The Hotels component should not fetch hotels when there are hotels in the scope', async () => {
  const mockFunction = jest.fn();
  const wrapper = ({ children }) => (
    <BrowserRouter>
      <HotelsContext.Provider
        value={{
          loading: false,
          error: '',
          hotels: [
            { id: 1, title: 'Test hotel 1', thumbnail: ''},
            { id: 2, title: 'Test hotel 2', thumbnail: ''},
          ],
          fetchHotels: mockFunction,
        }}
      >
        {children}
      </HotelsContext.Provider>
    </BrowserRouter>
  );
  render(<Hotels />, { wrapper });
  await waitFor(() => expect(mockFunction).not.toHaveBeenCalled());
});
