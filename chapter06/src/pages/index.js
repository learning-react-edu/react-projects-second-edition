import { HotelDetail } from './HotelDetail';
import { Hotels } from './Hotels';
import { ReviewForm } from './ReviewForm';

export {
  HotelDetail,
  Hotels,
  ReviewForm,
};
